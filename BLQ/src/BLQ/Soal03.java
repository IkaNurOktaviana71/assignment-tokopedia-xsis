package BLQ;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal03 {
    public static void main(String[] args) throws ParseException {
        //3.	Buatlah fungsi untuk kalkulasi tarif parkir berikut
        //Masuk	Keluar	Tarif
        //27 Januari 2019 | 05:00:01	27 Januari 2019 | 17:45:03
        //27 Januari 2019 | 07:03:59	27 Januari 2019 | 15:30:06
        //27 Januari 2019 | 07:05:00	28 Januari 2019 | 00:20:21
        //27 Januari 2019 | 11:14:23	27 Januari 2019 | 13:20:00
        //Ketentuan tariff:
        //-	8 jam pertama: 1.000/jam
        //-	8 jam 1 detik – 24 jam: 8.000 flat
        //-	24 jam 1 detik – 8 jam kemudian: 15.000 flat + 1.000/jam
        //Misalnya parkir selama 30 jam, maka tarifnya adalah:

        int detik = 1000;
        int menit= 60*detik;
        int jam = 60*menit;
        int hari = 60*jam;

        Scanner scanner = new Scanner(System.in);
        Locale locale = new Locale("id", "ID");
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", locale);

        System.out.print("Parkir masuk (day & hour): ");
        String masuk = scanner.nextLine();

        System.out.print("keluar parkir: ");
        String keluar = scanner.nextLine();

        //merubah string ke bentuk date
        Date parMasuk = sdf.parse(masuk);//merubah parkir masuk ke date
        Date perKeluar= sdf.parse(keluar);

        //mengambil waktu dari date
        long parkirMasuk = parMasuk.getTime();
        long parkirKeluar= perKeluar.getTime();
        long lamaParkir = parkirKeluar -parkirMasuk;
        long lamaJamParkir = lamaParkir/jam;
        System.out.println(lamaJamParkir);
        long bayarParkir=0;

        if(lamaJamParkir>24){
            bayarParkir = bayarParkir + 15_000 + (lamaJamParkir-24) * 1_000;
            System.out.println("bayar parkir sebesar: " + bayarParkir);
        }else if(lamaJamParkir>8 && lamaJamParkir<24){
            bayarParkir = bayarParkir +8_000;
            System.out.println("bayar parkir sebesar: " + bayarParkir);
        } else if (lamaJamParkir<=8) {
            bayarParkir = bayarParkir + 1_000;
            System.out.println("bayar parkir sebesar: " + bayarParkir);
        }


    }
}
