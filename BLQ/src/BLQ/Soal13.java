package BLQ;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class Soal13 {
    public static void main(String[] args) {
        //13.	Berapa derajat sudut terkecil yang dibentuk 2 jarum jam?
        //Jam 3:00  90
        //Jam 5:30  15
        //Jam 2.20  50
        //*detik tidak dipertimbangkan

        double sudut;
        Scanner scanner = new Scanner(System.in);
        System.out.println("masukan jam dan menit AM/PM CONTOH 03:00: ");
        String waktu = scanner.nextLine();
        String[] arr = waktu.split(":");
        if(Integer.parseInt(arr[1])==00 && Integer.parseInt(arr[0])<=6){
            sudut = Integer.parseInt(arr[0])*30;
        }else if (Integer.parseInt(arr[1])==00 && Integer.parseInt(arr[0])>=6) {
            sudut= 360 -(Integer.parseInt(arr[0])*30);
        }else{
            sudut = Integer.parseInt(arr[0])*30- (Integer.parseInt(arr[0])*60+Integer.parseInt(arr[1])*30)/60;
        }

        System.out.println(sudut);

    }
}
