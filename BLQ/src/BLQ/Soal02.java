package BLQ;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal02 {
    public static void main(String[] args) throws ParseException {

        //2.	Di perpustakaan, anda bias meminjam buku selama beberapa hari, durasinya berbeda-beda setiap buku tergantung dari konten buku tersebut. Jika terlambat mengembalikan, maka akan dikenakan denda sebesar 100/hari.
        //Berikut adalah contoh buku yang dipinjam seorang anggota perpustakaan
        //Nama Buku	Durasi Peminjaman (hari)
        //A	14
        //B	3
        //C	7
        //D	7
        //Buatlah kalkulasi penghitungan denda jika buku tersebut dipinjam pada rentang waktu
        //a.	28 Februari 2016 – 7 Maret 2016*
        //b.	29 April 2018 – 30 Mei 2018
        //Jika tidak ada denda, cukup tulis 0 (nol). Asumsikan perpustakaan buka setiap hari, tidak ada hari libur.
        //*Tahun 2016 adalah tahun kabisat


        int detik =1000;
        int menit = 60*detik;
        int jam = 60*menit;
        int hari = 24*jam;
        Scanner scanner = new Scanner(System.in);
        Locale locale = new Locale("id", "ID");
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", locale);

        System.out.print("tanggal pinjam: ");
        String pinj = scanner.nextLine();

        System.out.print("tanggal kembali: ");
        String kem = scanner.nextLine();

        //konversi String ke tanggal
        Date pinjam = sdf.parse(pinj);
        Date kembali = sdf.parse(kem);

        //konversi tanggal kemilis
        long tangPinjam = pinjam.getTime();
        long tangKembali = kembali.getTime();
        long lama = tangKembali -tangPinjam;
        long lamaPinjam = lama/hari;
        System.out.println(lamaPinjam);
        int denda =100;
        long totalDenda;

        long bukuA, bukuB, bukuC, bukuD;
        if(lamaPinjam > 14){
            bukuA = (lamaPinjam-14) * denda;
            bukuB = (lamaPinjam-3) * denda;
            bukuC = (lamaPinjam-7) *denda;
            bukuD= (lamaPinjam-7)*denda;
            totalDenda=bukuA+bukuB+bukuC+bukuD;
            System.out.print("total denda Rp" + totalDenda);
        }else if(lamaPinjam<14){
            bukuB = (lamaPinjam-3) * denda;
            bukuC = (lamaPinjam-7) *denda;
            totalDenda=+bukuB+(bukuC*2);
            System.out.print("total denda Rp" + totalDenda);
        }
    }
}
