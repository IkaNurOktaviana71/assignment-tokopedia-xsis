package BLQ;

import java.util.Scanner;

public class Soal09 {
    public static void main(String[] args) {
        //9.	N = 3 	 3 6 9
        //N = 4	 4 8 12 16
        //N = 5	 5 10 15 20 25

        Scanner scanner = new Scanner(System.in);
        System.out.print("masukan n: ");
        int n= scanner.nextInt();

        int nilai=n;
        for (int i = 0; i < n; i++) {
            System.out.print(nilai + " ");
            nilai= nilai + n;
        }
    }
}
