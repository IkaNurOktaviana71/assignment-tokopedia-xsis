package BLQ;

import java.util.Arrays;
import java.util.Comparator;

public class Soal08 {
    public static void main(String[] args) {
        //8.	Tentukan nilai minimal dan maksimal dari penjumlahan 4 komponen deret ini
        //1 2 4 7 8 6 9

        String deret = "1 2 4 7 8 6 9";
        String[] deret1 = deret.split(" ");
        String[] deret2 = deret.split(" ");
        int b=0;
        Arrays.sort(deret1);
        for (int i = 0; i < 4; i++) {
//            System.out.print(deret1[i]);
            int a = Integer.parseInt(deret1[i]);
            b= b+a;
        }
        System.out.println("nilai min " + b);
        Arrays.sort(deret2, Comparator.reverseOrder());
        int c=0;
        for (int i = 0; i < 4; i++) {
//            System.out.println(deret2[i]);
            int a= Integer.parseInt(deret2[i]);
            c=c+a;
        }

        System.out.println("nilai max " + c);

    }
}
