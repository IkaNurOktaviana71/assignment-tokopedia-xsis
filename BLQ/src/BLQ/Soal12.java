package BLQ;

import java.util.Scanner;

public class Soal12 {
    public static void main(String[] args) {
        //12.	Input: 1 2 1 3 4 7 1 1 5 6 1 8
        //Output: 1 1 1 1 1 2 3 4 5 6 7 8
        //*selesaikan dengan TIDAK menggunakan fungsi Sort

        Scanner scanner = new Scanner(System.in);

        System.out.println("contoh deret angka yang ingin diurutkan : 4 2 3 1");
        System.out.println("input angka: ");
        String deretAngka = scanner.nextLine();

        String[] strAngka = deretAngka.split(" ");
        int[] arrAngka = new int[strAngka .length];

        for (int i = 0; i < strAngka .length; i++) {
            arrAngka[i] = Integer.valueOf(strAngka [i]);
        }

        for (int i = 0; i < arrAngka.length; i++) {
            int index = i;
            while (index > 0 && arrAngka[index] < arrAngka[index - 1]) {
                int angka = arrAngka[index];
                arrAngka[index] = arrAngka[index - 1];
                arrAngka[index - 1] = angka;
                index--;
            }
        }

        for (int i = 0; i < arrAngka.length; i++) {
            System.out.print(arrAngka[i] + " ");
        }
    }
}
