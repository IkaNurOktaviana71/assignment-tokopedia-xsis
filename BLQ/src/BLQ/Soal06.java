package BLQ;

import java.util.Scanner;

public class Soal06{
    public static void main(String[] args) {
        //6.	Tanpa menggunakan fungsi Reverse, buatlah fungsi untuk menentukan apakah sebuah kata adalah palindrome* atau tidak
        //*Palindrome adalah kata yang jika dibalik tetap sama, contohnya “katak”, “12021”, “malam”

        Scanner scanner = new Scanner(System.in);
        System.out.print("masukan kalimat: ");
        String[] kalimat = scanner.nextLine().toLowerCase().replace(" ","").split("");
        String palindrom = "";
        String kal="";

        for (int i = kalimat.length-1; i >= 0 ; i--) {
            palindrom = palindrom+ kalimat[i];
        }
        System.out.print(palindrom);

        System.out.println();
        for (int i = 0; i < kalimat.length; i++) {
            kal = kal + kalimat[i];
        }
        System.out.println(kal);

        if(kal.equalsIgnoreCase(palindrom)){
            System.out.println("palindrom");
        }else{
            System.out.println("not palindrom");
        }

    }
}
