package BLQ;

import java.util.Arrays;
import java.util.Scanner;

public class Soal19 {
    public static void main(String[] args) {
        //19.	Tentukan apakah kalimat ini adalah Pangram* atau bukan
        //“Sphinx of black quartz, judge my vow”
        //“Brawny gods just flocked up to quiz and vex him”
        //“Check back tomorrow; I will see if the book has arrived.”
        //*Pangram adalah kata atau kalimat yang mengandung setiap abjad alphabet,
        //contohnya “A quick brown fox jumps over the lazy dog”

        Scanner scanner = new Scanner(System.in);
        System.out.print("masukan kalimat: ");
        String kalimat = scanner.nextLine();

        kalimat = kalimat.toLowerCase().replace(" ","").replaceAll("[^a-zA-Z]", "");
        String[] arrKal= kalimat.split("");
        String b="";

        Arrays.sort(arrKal);
        for (int i = 0; i < arrKal.length; i++) {
            if(!b.contains(arrKal[i])){
                b= b + arrKal[i];
            }
        }
        System.out.println(b);
        if(b.length()==26){
            System.out.println("Pangram");
        }else {
            System.out.println("Bukan Pangram");
        }

    }
}
