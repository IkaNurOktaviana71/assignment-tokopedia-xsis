package BLQ;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal15 {
    public static void main(String[] args) throws ParseException {
        //15.	Ubah format waktu "03:40:44 PM" menjadi format 24 jam (15:40:44)

        Locale locale = new Locale("id", "ID");
        String jam = "03:40:44 PM";


        SimpleDateFormat sdf_24 = new SimpleDateFormat("HH:mm:ss ", locale);
        SimpleDateFormat sdf_12 = new SimpleDateFormat("hh:mm:ss aa", locale);

        //mengkonversi jam 12 jam
        Date _12jam = sdf_12.parse(jam);

        //mengubah format 12 jam menjadi 24 jam
        System.out.println(sdf_24.format(_12jam));

    }
}
